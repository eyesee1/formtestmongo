Minimal example of Flask + MongoDB + Forms

All the action happens in formtest/formtest.py

## setup

```bash
$ cd formtestmongo
$ python2.7 bootstrap.py -d -v 2.1.1
```
Specifying the version of buildout in that command is important—there is currently a version conflict with newer versions of zc.buildout vs. the version of setuptools/distribute on wheezy.

That will download packages, and create some folders and stuff. Then:

`$ bin/buildout`

This will download all the dependencies.

## to run

`$ bin/run`

## interactive shell

`$ bin/shell`

