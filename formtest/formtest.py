# -*- coding: utf-8 -*-

from __future__ import with_statement, print_function

from pprint import pprint, pformat
import datetime 

import werkzeug.script as wscript

from flask_wtf import Form
from wtforms.widgets import HiddenInput
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields.html5 import URLField, EmailField
from wtforms_components import PhoneNumberField
from wtforms.validators import Required, Length, Email, URL, Optional, ValidationError
from wtforms.fields import StringField, BooleanField, DateField, TextAreaField,       \
                      PasswordField, RadioField, HiddenField, FormField, FieldList,    \
                      IntegerField, Field, DecimalField, DateTimeField

from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

from pymongo import MongoClient

# configuration
MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DB_NAME = 'pyformtester'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

class MetaDataForm(Form):
    csrf_enabled = False
    date = DateTimeField(default=datetime.datetime.now())

class EntryForm(Form):
    title = StringField(u"Title", validators=[Required()])
    text = TextAreaField(u"Text", validators=[Optional()])
    metadata = FormField(MetaDataForm)

# create our little application :)
app = Flask(__name__)
connection = MongoClient(MONGO_HOST, port=MONGO_PORT, tz_aware=True)
db = connection[MONGO_DB_NAME]
coll = db['entries']

app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


@app.route('/')
def show_entries():
    form = EntryForm()
    return render_template(
        'show_entries.html', 
        entries=coll.find(),
        form=form
    )


@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    form = EntryForm()
    if form.validate_on_submit():
        print("Form validated!")
        pprint(form.__dict__)
        print("-"*70)
        print(form.data)
        entry = dict(**form.data)
        coll.insert(entry)
        flash('New entry was successfully posted')
    else:
        print("Form did not validate. :(")
        pprint(form.__dict__)
        return render_template(
            'show_entries.html', 
            entries=coll.find(),
            form=form
        )
    return redirect(url_for('show_entries'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))

def make_shell_context():
    global app, db, coll
    with app.app_context():
        return dict(
            app=app,
            db=db,
            coll=coll
        )

def make_shell():
    with app.app_context():
        wscript.make_shell(make_shell_context, use_ipython=True)()

def run_app():
    app.run()

if __name__ == '__main__':
    app.run()

