from setuptools import setup, find_packages
import sys, os

version = '0.0.1'

setup(name='formtest',
      version=version,
      description="""
      test simple forms flask + mongodb + wtforms. based on a flask example blogging app.
      """,
      long_description="""\
write long description here...""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='python formtest mongodb',
      author='Isaac Csandl',
      author_email='isaac.csandl@wrapports.com',
      url='',
      license='Proprietary',
      # packages=find_packages(),
      packages=['formtest'],
      package_dir={
          'formtest':'formtest',
      },
      package_data={'': ['README', '*.md', '*.txt', ]},
      entry_points=("""
          [console_scripts]
          run=formtest:run_app
          shell=formtest:make_shell
      """ ),
      include_package_data=True,
      install_requires=[
          # -*- Extra requirements: -*-
          "pymongo",
          "flask",
          "flask-wtf",
          "wtforms-components",
      ],
)
